import 'dart:async';
import 'package:dd_app/viewgrocery.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:go_router/go_router.dart';
import 'home.dart';
import 'createuser.dart';
import 'profile.dart';
import 'addrecipe.dart';
import 'allrecipes.dart';
import 'viewrecipe.dart';
import 'editrecipe.dart';
import 'sharedrecipes.dart';
import 'viewsharedecipe.dart';

class MyColors
{
  static const MainColor = Color.fromRGBO(146, 182, 61, 1);
  static const AccentColor = Color.fromRGBO(100, 120, 57, 1);
  static const recipeColor = Color.fromRGBO(195, 212, 155, 1);
}

class MyTextTheme extends TextTheme
{
  const MyTextTheme()
  : super(
    titleSmall: const TextStyle(
      fontFamily: 'RubikMonoOne',
    ),
    titleLarge: const TextStyle(fontFamily: 'Chewy'),
    titleMedium: const TextStyle(
      fontFamily: 'Chewy',
      fontSize: 25,
    )
  );
 }

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}


final _router = GoRouter(
  initialLocation: '/login',
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) 
      {
        return FirebaseAuth.instance.currentUser == null
        ? const AuthPage(title: 'Log In')
        : const MyHomePage(title: 'Home');
      },
    ),
      GoRoute(
        path: '/login',
        builder: (context, state) => const AuthPage(title: 'Log In'),
      ),
    GoRoute(
      path: '/createuser',
      builder:(context, state) => const CreateUser(title: 'Create an Account'),
    ),
    GoRoute(
      path: '/profile',
      builder: (context, state)
      {
        return FirebaseAuth.instance.currentUser == null
        ? const AuthPage(title: 'Log In')
        : const ProfilePage(title: 'Profile');
      },
    ),
    GoRoute(
      path: '/addrecipe',
      builder: (context, state)
      {
        return FirebaseAuth.instance.currentUser == null
        ? const AuthPage(title: 'Log In')
        : const AddRecipePage(title: 'Add New Recipe');
      }
    ),
    GoRoute(
      path: '/allrecipes',
      builder: (context, state)
      {
        return FirebaseAuth.instance.currentUser == null
        ? const AuthPage(title: 'Log In')
        : const AllRecipesPage(title: 'All Recipes');
      }
      ),
      GoRoute(
        path: '/viewrecipe/:id',
        builder: (BuildContext context, GoRouterState state)
        {
          final recipeId = state.pathParameters['id']!;
          return ViewRecipePage(recipeId: recipeId);
        }
      ),
      GoRoute(
        path: '/editrecipe/:id',
        builder: (BuildContext context, state) {
          final recipeId = state.pathParameters['id']!;
          return EditRecipePage(recipeId: recipeId);
        }
      ),
      GoRoute(
        path: '/viewgrocery',
        builder: (context, state) {
          return FirebaseAuth.instance.currentUser == null
          ? const AuthPage(title: 'Log In')
          : const ViewGroceryPage(title: 'Grocery List');
        }
      ),
      GoRoute(
        path: '/sharedrecipes',
        builder: (context, state)
        {
          return FirebaseAuth.instance.currentUser == null
          ? const AuthPage(title: 'Log In')
          : const SharedRecipesPage(title: 'Shared Recipes');
        }
      ),
      GoRoute(
        path: '/viewsharedrecipe/:id',
        builder: (BuildContext context, GoRouterState state)
        {
          final recipeId = state.pathParameters['id']!;
          return ViewSharedRecipePage(recipeId: recipeId);
        }
      ),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: const MyTextTheme(),
        colorScheme: ColorScheme.fromSeed(seedColor: MyColors.MainColor),
        useMaterial3: true,
      ),
      routerConfig: _router,
    );
  }
}

class AuthPage extends StatefulWidget 
{
  const AuthPage({super.key, required this.title});
  final String title;

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage>
{
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _pwdController = TextEditingController();

  Future<void> _login() async
  {
    try
    {
      if(_formKey.currentState != null && _formKey.currentState!.validate())
      {
        await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: _emailController.text,
          password: _pwdController.text,
        );
        print('sign in working.');
        if(mounted)
        {
          context.go('/');
        }
      }
    }
    catch(e)
    {
      const SnackBar(content: Text('failed to sign in.'));
      print(e);
    }  
  }

  @override Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title,
        style: Theme.of(context).textTheme.titleLarge
        ),
      ),
      body: SingleChildScrollView(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/logo_letter.png',
              height: 250,
              fit: BoxFit.contain,
            ),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: Theme.of(context).textTheme.titleSmall, 
                      icon: const Icon(Icons.person),
                    ),
                    validator: (value) 
                    {
                      if(value == null || value.isEmpty)
                      {
                        return 'Please enter your email address.';
                      }
                      return null;
                    },
                  ),
                  TextFormField(
                    controller: _pwdController,
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    decoration: InputDecoration(
                      labelText: 'Password',
                      labelStyle: Theme.of(context).textTheme.titleSmall,
                      icon: const Icon(Icons.lock),
                    ),
                    validator: (value)
                    {
                      if(value == null || value.isEmpty)
                      {
                        return 'Please enter a password.';
                      }
                      return null;
                    },
                  ),
                ],
              ),
            ),
            ElevatedButton(
              onPressed: _login, 
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)),
                child: const Text('Log In'),
            ),
            TextButton(
              onPressed: () => context.go('/createuser'),
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
              ),
              child: const Text('Create an Account'),
            ),
          ],
        ),
      ),
      ),
    );
  }
}
