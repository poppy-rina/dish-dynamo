 import 'package:dd_app/main.dart';
 import 'package:flutter/material.dart';
 import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:firebase_auth/firebase_auth.dart';
 import 'package:go_router/go_router.dart';
 
 class ViewSharedRecipePage extends StatefulWidget
 {
  const ViewSharedRecipePage({super.key, required this.recipeId});
  final String recipeId;
  final String title = 'View Shared Recipe';

  @override
  State<ViewSharedRecipePage> createState() => _ViewSharedRecipePageState();
 }

class _ViewSharedRecipePageState extends State<ViewSharedRecipePage>
{

 Future <void> _addItem(BuildContext context, String item, String recipe) async
  {
    final currentUser = FirebaseAuth.instance.currentUser;
    if(currentUser == null)
    {
      return;
    }

    final snapshot = await FirebaseFirestore.instance
      .collection('users')
      .doc(currentUser.uid)
      .collection('groceryList')
      .doc(item).get();

    String message;
    if(!snapshot.exists)
    { 
        FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('groceryList')
          .doc(item).set({'item': item, 'recipes': [recipe]});
        message = 'Item and recipe added to grocery list.';
    }
    else
    {
      List<dynamic> existing = snapshot.data()!['recipes'] as List;
      
      if(existing.contains('None'))
      {
        FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('groceryList')
          .doc(item).set({'item': item, 'recipes': [recipe]});
        message = 'Recipe added to existing grocery item.';
      }
      else if(!existing.contains(recipe))
      {
        existing.add(recipe);
        FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('groceryList')
          .doc(item)
          .update({'recipes': existing});
        message = 'Recipe added to existing grocery item.';
      }
      else
      {
        message = 'Recipe already exists for grocery item';
      }
    }

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: const Duration(seconds: 3),
        ));
  }


  @override
  Widget build(BuildContext context)
  {
    final currentUser = FirebaseAuth.instance.currentUser;
    if(currentUser == null)
    {
      return const Scaffold(
        body: Center(
          child: Text('User not logged in'),
        )
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: StreamBuilder
      (
        stream: FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('sharedRecipes')
          .doc(widget.recipeId).snapshots(),
        builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot)
        {
          if(snapshot.connectionState == ConnectionState.waiting)
          {
            return const Center(child: CircularProgressIndicator());
          }

          if(snapshot.hasError)
          {
            return Center(child: Text('Error: ${snapshot.error}'));
          }

          final recipeData = snapshot.data?.data() as Map<String, dynamic>?;

          if(recipeData == null || recipeData['status'] != 'accepted')
          {
            return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: ()  async
                      {
                        await FirebaseFirestore.instance
                          .collection('users')
                          .doc(currentUser.uid)
                          .collection('sharedRecipes')
                          .doc(widget.recipeId)
                          .update({'status': 'accepted'});

                        setState((){});
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      child: const Text('Accept')
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        await FirebaseFirestore.instance
                          .collection('users')
                          .doc(currentUser.uid)
                          .collection('sharedRecipes')
                          .doc(widget.recipeId).delete();
                        context.go('/sharedrecipes');
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      child: const Text('Delete')
                    )
                  ]
                ),
              )]
            ));
          }

          final imageUrl = recipeData['imageUrl'] ?? 'assets/logo.png';
          final recipeTitle = recipeData['title'] ?? 'Untitled';
          final sender = recipeData['sharedBy'] ?? 'Unknown';
          final List<String> ingredients = List<String>.from(recipeData['ingredients'] ?? []);
          final List<String> instructions = List<String>.from(recipeData['instructions'] ?? []);

              return SingleChildScrollView(
                child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  ElevatedButton(
                    onPressed: () => context.go('/sharedrecipes'),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  child: const Text('All Shared Recipes')
                ),
                imageUrl != 'assets/logo.png'
                  ? Image.network(
                    imageUrl,
                    width: 200,
                    height: 200,
                    fit: BoxFit.cover,
                  )
                  : Image.asset(
                    imageUrl,
                    height: 200,
                    width: 200,
                    fit: BoxFit.cover,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center, 
                      children: [
                        Text(
                          recipeTitle,
                          style: Theme.of(context).textTheme.titleMedium,
                        textAlign: TextAlign.center,
                        ),
                      ]
                    ),
                ),
                const SizedBox(height: 20),
                Text(
                  'Shared by: $sender',
                  style: const TextStyle(
                    fontFamily: 'PTSansNarrow',
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                )),
                const SizedBox(height: 20),
                Container(
                  margin: const EdgeInsets.all(8),
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: MyColors.recipeColor,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                      'Ingredients',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                    const SizedBox(height: 8),
                    for(String item in ingredients)
                    Row(
                      children: [
                        Text('- $item',
                        style: const TextStyle(fontFamily: 'PTSansNarrow',
                          fontWeight: FontWeight.bold
                        )),
                        IconButton(
                          onPressed: ()
                          {
                            _addItem(context, item, recipeTitle);
                          },
                          icon:const Icon(Icons.add),
                        )
                      ]
                    )
                    ]
                  )
                ),
                const SizedBox(height: 20),
                Text(
                      'Instructions',
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                    for(int i = 0; i < instructions.length; i++)
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text('${i+1}. ${instructions[i]}',
                      style: const TextStyle(
                        fontFamily: 'PTSansNarrow',
                        fontWeight: FontWeight.bold
                      )),
                    ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: ()
                      {
                        showDialog(
                          context: context,
                          builder: (BuildContext context)
                          {
                            return AlertDialog(
                              title: const Text('Delete Recipe'),
                              content: const Text('Are you sure you want to delete this recipe?'),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: ()
                                  {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text('Cancel'),
                                ),
                                TextButton(
                                  onPressed: () async 
                                  {
                                    try
                                    {
                                      await FirebaseFirestore.instance
                                        .collection('users')
                                        .doc(currentUser.uid)
                                        .collection('sharedRecipes')
                                        .doc(recipeTitle).delete();
                                      Navigator.of(context).pop();
                                      ScaffoldMessenger.of(context).showSnackBar(
                                        const SnackBar(
                                          content: Text('Recipe deleted successfully')
                                        ),
                                      );
                                      context.go('/sharedrecipes');
                                    }
                                    catch(e)
                                    {
                                      ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                          content: Text('Failed to delete recipe: $e'), 
                                        ),
                                      );
                                    }
                                  },
                                  child: const Text('Delete'),
                                )
                              ],
                            );
                          }
                        );
                      },
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      child: const Text('Delete')
                    ),
                  ],
                )
              ],
              ));
        },
      ),
    );
  }
}