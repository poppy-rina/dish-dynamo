 import 'dart:async';
 import 'dart:io';
 import 'package:dd_app/main.dart';
 import 'package:flutter/material.dart';
 import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:firebase_auth/firebase_auth.dart';
 import 'package:image_picker/image_picker.dart';
 import 'package:firebase_storage/firebase_storage.dart';
 import 'package:go_router/go_router.dart';

  class EditRecipePage extends StatefulWidget
 {
  const EditRecipePage({super.key, required this.recipeId});
  final String title = 'Edit Recipe';
  final String recipeId;

  @override
  State<EditRecipePage> createState() => _EditRecipePageState();
 }

class _EditRecipePageState extends State<EditRecipePage>
{
  final _formKey = GlobalKey<FormState>();
  final _recipeNameController = TextEditingController();
  final List<TextEditingController> _ingredientControllers = [];
  final List<TextEditingController> _stepControllers = [];
  String imageUrl = 'assets/logo.png';
  File? _image;

  @override
  void initState()
  {
    super.initState();
    _fetchRecipeData();
  }

  Future<void> _fetchRecipeData() async
  {
    try
    {
      final snapshot = await FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('recipes')
      .doc(widget.recipeId).get();
      if(snapshot.exists)
      {
        final recipeData = snapshot.data() as Map<String, dynamic>;
        _recipeNameController.text = recipeData['title'] ?? '';
        final List<String>? ingredients = recipeData['ingredients']?.cast<String>();
        final List<String>? instructions = recipeData['instructions']?.cast<String>();
        imageUrl = recipeData['imageUrl'];

        if(ingredients != null)
        {
          _ingredientControllers.clear();
          _ingredientControllers.addAll(ingredients.map((ingredient) => TextEditingController(text: ingredient)));
        }

        if(instructions != null)
        {
          _stepControllers.clear();
          _stepControllers.addAll(instructions.map((step) => TextEditingController(text: step)));
        }
        setState(() {});
      }
      else
      {
        const Center(child: Text('Recipe not found.'));
      }
    }
    catch(e)
    {
      Center(child: Text('Error: $e'));
    }
  }

  Future<void> _selectImage() async
  {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context)
      {
        return SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: const Icon(Icons.camera_alt),
                title: const Text('Take a Photo'),
                onTap: ()
                {
                  Navigator.pop(context);
                  _getImageFromCamera();
                },
              ),
              ListTile(
                leading: const Icon(Icons.photo_library),
                title: const Text('Choose from Gallery'),
                onTap: ()
                {
                  Navigator.pop(context);
                  _getImageFromGallery();
                }
              )
            ]
          )
        );
      }
    );
  }

  Future<void> _getImageFromCamera() async 
  {
    final curFile = await ImagePicker().pickImage(source: ImageSource.camera);
    if(curFile != null)
    {
      setState(() {
        _image = File(curFile.path);
      });
    }
  }

  Future<void> _getImageFromGallery() async
  {
    final curFile = await ImagePicker().pickImage(source: ImageSource.gallery);
    if(curFile != null)
    {
      setState(() {
        _image = File(curFile.path);
      });
    }
  }

  Future<void> _saveRecipe() async
  {
    final user = FirebaseAuth.instance.currentUser;
    if(user == null)
    {
      return;
    }

    final recipeTitle = _recipeNameController.text.trim();

    if(_formKey.currentState!.validate())
    {
      try
      {
          if(_image != null)
          {
            final storageRef = FirebaseStorage.instance.ref().child('recipe_images').child('$recipeTitle.jpg');
            await storageRef.putFile(_image!);
            imageUrl = await storageRef.getDownloadURL();
          }

          final List<String> ingredients = _ingredientControllers.map((controller) => controller.text).toList();
          final List<String> instructions = _stepControllers.map((controller) => controller.text).toList();

          await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .collection('recipes')
          .doc(recipeTitle).set({
            'userId': user.uid,
            'title': recipeTitle,
            'ingredients': ingredients,
            'instructions': instructions,
            'imageUrl' : imageUrl,
          });
          context.go('/viewrecipe/$recipeTitle');
      }
      catch(e)
      {
        print('Error saving recipe: $e');
      }
    }
  }


  void _addIngredient()
  {
    setState(() {
      _ingredientControllers.add(TextEditingController());
    });
  }

  void _addStep()
  {
    setState(() {
      _stepControllers.add(TextEditingController());
    });
  }
  Widget _buildIngredients()
  {
    List<Widget> ingredientsFields = _ingredientControllers.map((controller)
    {
      return Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: controller,
              decoration: const InputDecoration(labelText: 'Ingredient'),
              validator: (value)
              {
                if(value == null || value.isEmpty)
                {
                  return 'please enter an ingredient';
                }
                return null;
              },
            ),
          ),
        ],
      );
    }).toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
         Text(
          'Ingredients:',
          style: Theme.of(context).textTheme.titleSmall
          ),
          const SizedBox(height: 10),
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: ingredientsFields,
          ),
          const SizedBox(height: 10),
          TextButton(
          onPressed: _addIngredient, 
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
          ),
          child: const Text('+ Add Ingredient'),
        ),
      ],
    );
  }

  Widget _buildInstructions()
  {
    List<Widget> stepFields = _stepControllers.map((controller)
    {
      return Row(
        children: [
          Expanded(
            child: TextFormField(
              controller: controller,
              decoration: const InputDecoration(labelText: 'Step'),
              validator: (value)
              {
                if(value == null || value.isEmpty)
                {
                  return 'Please enter a step';
                }
                return null;
              },
            ),
          ),       
        ],
      );
    }).toList();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
         Text(
          'Instructions:',
          style: Theme.of(context).textTheme.titleSmall,
        ),
        const SizedBox(height: 10),
        ListView(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          children: stepFields,
        ),
        const SizedBox(height: 10),
        TextButton(
          onPressed: _addStep, 
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
          ),
          child: const Text('+ Add Step'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                  controller: _recipeNameController,
                  decoration: const InputDecoration(labelText: 'Recipe Name'),
                  validator: (value)
                  {
                    if(value == null || value.isEmpty)
                    {
                      return 'please enter a recipe name';
                    }
                    return null;
                  },
                ),
                _image is File
                ? Image.file(
                  _image!,
                  height: 200,
                  width: 200,
                  fit: BoxFit.cover,
                )
                :imageUrl == 'assets/logo.png' 
                ? Image.asset(
                  imageUrl,
                  height: 200,
                  width: 200,
                )
                : Image.network(
                  imageUrl,
                  height: 200,
                  width: 200,
                  fit: BoxFit.cover,
                ),
                IconButton(
                  onPressed: _selectImage,
                  icon: const Icon(Icons.camera_alt),
                  tooltip: 'Take a photo',
                ),
                const SizedBox(height: 20),
                _buildIngredients(),
                const SizedBox(height: 20),
                _buildInstructions(),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () => _saveRecipe(),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                    foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  child: const Text('Save Recipe'),
                ),
              ],
            ),
          ),
        )
      ),
    );
  }
}