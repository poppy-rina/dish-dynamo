import 'main.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:firebase_auth/firebase_auth.dart';


class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Future<List<QueryDocumentSnapshot<Map<String, dynamic>>>> _getRandomRecipes() async
  {
    QuerySnapshot<Map<String, dynamic>> snapshot = await FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('recipes')
      .get();
    
    if(snapshot.docs.length >= 2)
    {
      List<QueryDocumentSnapshot<Map<String, dynamic>>> shuffledRecipes = snapshot.docs..shuffle();
      return shuffledRecipes.sublist(0,2);
    }
    else
    {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title,
        style: Theme.of(context).textTheme.titleLarge
        ),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              Image.asset(
                'assets/logo_letter.png',
                height: 200,
                fit: BoxFit.contain,
              ),
              FutureBuilder<List<QueryDocumentSnapshot<Map<String, dynamic>>>>(
                future: _getRandomRecipes(),
                builder: (context, snapshot)
                {
                  if(snapshot.connectionState == ConnectionState.waiting)
                  {
                    return const CircularProgressIndicator();
                  }
                  if(snapshot.connectionState == ConnectionState.waiting)
                  {
                    return const Center(child: CircularProgressIndicator());
                  }

                  if(snapshot.hasError)
                  {
                    return Center(child: Text('Error: ${snapshot.error}'));
                  }

                  final recipes = snapshot.data ?? [];

                  if(recipes.isEmpty)
                  {
                    return const Center(child: Text('Not Enough Recipes Yet'));
                  }

                  return GridView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                    itemCount: recipes.length,
                    itemBuilder: (context, index)
                    {
                      final doc = recipes[index];
                      final recipe = doc.data();
                      final imageUrl = recipe['imageUrl'] ?? 'assets/logo.png';
                      final recipeTitle = recipe['title'] ?? 'Untitled';

                      return GestureDetector(
                        onTap: () {
                          context.go('/viewrecipe/${doc.id}');
                        },
                        child : Card(
                        color: MyColors.recipeColor,
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: imageUrl != 'assets/logo.png' 
                              ? Image.network(
                                imageUrl,
                                width: 100,
                                height: 100,
                                fit: BoxFit.cover,
                              ) 
                              : Image.asset(
                                imageUrl,
                                height: 100,
                                width: 100,
                                fit: BoxFit.cover,
                              ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              recipeTitle != '' 
                              ? recipeTitle
                              : 'Untitled',
                              style: Theme.of(context).textTheme.titleSmall,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text('View Full Recipe...'),
                          ),
                        ],
                      )));
                    }
                  );
                }
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () => context.go('/allrecipes'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      child: const Text('View Recipes')
                    ),
                    ElevatedButton(
                      onPressed: () => context.go('/viewgrocery'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                      ),
                      child: const Text('View Grocery')
                    )
                  ]
                ),
              ),
              Container(
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: MyColors.recipeColor,
                borderRadius: BorderRadius.circular(10.0),
              ),
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Grocery List',
                      style: Theme.of(context).textTheme.titleSmall,
                    ), 
                    StreamBuilder<QuerySnapshot> (
                      stream: FirebaseFirestore.instance
                        .collection('users')
                        .doc(FirebaseAuth.instance.currentUser!.uid)
                        .collection('groceryList').snapshots(),
                      builder: (context, snapshot)
                      {
                        if(snapshot.hasError)
                        {
                          return Text('Error: ${snapshot.error}');
                        }
                        else if(!snapshot.hasData)
                        {
                          return const Center(child: Text('No groceries yet'));
                        }
                        final groceryItems = snapshot.data!.docs;
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: groceryItems.length >= 4 
                              ? 4
                              : groceryItems.length,
                            itemBuilder: (context, index)
                            {
                            var groceryItem = groceryItems[index].data() as Map<String, dynamic>;
                            var itemName = groceryItem['item'] as String;
                            return CheckboxListTile(
                              title: Text(itemName,
                              style: const TextStyle(
                                fontFamily: 'PTSansNarrow',
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0,
                              )),
                              value: false,
                              onChanged: (bool? newVal)
                              {
                              if(newVal!)
                              {
                                FirebaseFirestore.instance
                                  .collection('users')
                                  .doc(FirebaseAuth.instance.currentUser!.uid)
                                  .collection('groceryList')
                                  .doc(itemName)
                                  .delete().catchError((error) => print('failed to delete document: $error'));
                                }
                              }
                            );
                          }
                        );
                      }
                    )
                ])
              )]
          ),
        )
      )
    );
  }
}
