 import 'package:dd_app/main.dart';
 import 'package:flutter/material.dart';
 import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:firebase_auth/firebase_auth.dart';
 import 'package:go_router/go_router.dart';
 
 class SharedRecipesPage extends StatefulWidget
 {
  const SharedRecipesPage({super.key, required this.title});
  final String title;

  @override
  State<SharedRecipesPage> createState() => _SharedRecipesPageState();
 }

class _SharedRecipesPageState extends State<SharedRecipesPage>
{
  @override
  Widget build(BuildContext context)
  {
    final currentUser = FirebaseAuth.instance.currentUser;
    if(currentUser == null)
    {
      return const Scaffold(
        body: Center(
          child: Text('User not logged in'),
        )
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: SingleChildScrollView(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ElevatedButton(
              onPressed: () => context.go('/profile'),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            ),
          child: const Text('Return to profile')
        ),
      StreamBuilder(
          stream: FirebaseFirestore.instance
            .collection('users')
            .doc(currentUser.uid)
            .collection('sharedRecipes')
            .snapshots(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot)
          {
            if(snapshot.connectionState == ConnectionState.waiting)
            {
              return const Center(child: CircularProgressIndicator());
            }

            if(snapshot.hasError)
            {
              return Center(child: Text('Error: ${snapshot.error}'));
            }

            final recipes = snapshot.data?.docs ?? [];

            if(recipes.isEmpty)
            {
              return const Center(child: Text('No Shared Recipes Yet'));
            }

            return GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemCount: recipes.length,
              itemBuilder: (context, index)
              {
                final doc = recipes[index];
                final recipe = doc.data() as Map<String, dynamic>;
                final imageUrl = recipe['imageUrl'] ?? 'assets/logo.png';
                final status = recipe['status'] ?? 'pending';
                final recipeTitle = recipe['title'] ?? 'Untitled';
                final sender = recipe['sharedBy'] ?? 'Unknown';

                return GestureDetector(
                  onTap: () {
                    context.go('/viewsharedrecipe/${doc.id}');
                  },
                child : Card(
                  color: MyColors.recipeColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Expanded(
                        child: imageUrl != 'assets/logo.png' 
                        ? Image.network(
                        imageUrl,
                        width: 100,
                        height: 100,
                        fit: BoxFit.cover,
                      ) 
                      : Image.asset(
                        imageUrl,
                        height: 100,
                        width: 100,
                        fit: BoxFit.cover,
                      ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          recipeTitle != '' 
                          ? recipeTitle
                          : 'Untitled',
                          style: Theme.of(context).textTheme.titleSmall,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Shared By: $sender',
                          style: const TextStyle(
                            fontFamily: 'PTSansNarrow',
                            fontWeight: FontWeight.bold,
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: status == 'accepted'
                        ? const Text(
                          'View Full Recipe...',
                          style: TextStyle(
                            fontFamily: 'PTSansNarrow'
                        ))
                        : const Text(
                          'View to Accept...',
                          style: TextStyle(
                            fontFamily: 'PTSansNarrow',
                        ))
                      ),
                    ],
                  ),
                )
                );
              },
            );
          },
        ),
      ],
      ),
      ),
    );
  }
}
