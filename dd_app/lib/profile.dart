 import 'dart:async';
 import 'package:flutter/material.dart';
 import 'package:firebase_auth/firebase_auth.dart';
 import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:go_router/go_router.dart';
 import 'main.dart';

 class ProfilePage extends StatefulWidget
 {
  const ProfilePage({super.key, required this.title});
  final String title;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
 }

 class _ProfilePageState extends State<ProfilePage>
 {

  Stream<List<Map<String, dynamic>>> getFriendsStream()
  {
    return FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('friends')
      .snapshots()
      .map((snapshot) => snapshot.docs
        .map((doc) => {
          'id': doc.id,
          'name': doc.data()['name'] as String,
          'status': doc.data()['status'] as String? ?? 'accepted',
          'type': doc.data()['type'] as String? ?? 'received',
        })
        .toList());
  }

  Future<void> _addFriend(String friend) async
  {
    if(friend.isEmpty) return;

    var currentUserRef = FirebaseFirestore.instance
    .collection('users')
    .doc(FirebaseAuth.instance.currentUser!.uid);

    var documentSnap = await FirebaseFirestore.instance
    .collection('users')
    .where('username', isEqualTo: friend).get();

    if(documentSnap.docs.isNotEmpty)
    {
      var friendId = documentSnap.docs.first.id;

      await currentUserRef
        .collection('friends')
        .doc(friendId)
        .set({
          'name' : friend, 
          'status': 'pending', 
          'type': 'sent'
        });

      await FirebaseFirestore.instance
        .collection('users')
        .doc(friendId)
        .collection('friends')
        .doc(currentUserRef.id)
        .set({
          'name': FirebaseAuth.instance.currentUser!.displayName,
          'status': 'pending',
          'type': 'received',  
        });
    }
    else 
    {
      showDialog(
        context: context,
        builder: (BuildContext context)
        {
          return AlertDialog(
            title: const Text('Username Not Found'),
            content: Text('The username $friend wasnt not found.'),
            actions: <Widget>[
              TextButton(
                onPressed: ()
                {
                  Navigator.of(context).pop();
                },
                child: const Text('OK')
              )
            ] 
          );
        }
      );
    }
  }

  Future<void> _deleteFriend(String id) async
  {
    await FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid)
      .collection('friends')
      .doc(id)
      .delete();
  }

  Future<void> _approveFriend(String id) async
  {
    var currentUserRef = FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid);

    await currentUserRef
      .collection('friends')
      .doc(id)
      .update({
        'status': 'accepted',
        'type': 'friend',
      });

    await FirebaseFirestore.instance
      .collection('users')
      .doc(id)
      .collection('friends')
      .doc(currentUserRef.id)
      .update({
        'status': 'accepted',
        'type': 'friend',
      });
    
    setState((){});
  }

  void _showAddFriendDialog()
  {
    TextEditingController _friendController = TextEditingController();
    showDialog(
      context: context,
      builder: (BuildContext context)
      {
        return AlertDialog(
          title: const Text('Add Friend'),
          content: TextField(
            controller: _friendController,
            decoration: const InputDecoration(
              hintText: 'Enter friend\'s username',
              hintStyle: TextStyle(
                fontFamily: 'PTSansNarrow'
              ),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Add'),
              onPressed: ()
              {
                _addFriend(_friendController.text);
                Navigator.of(context).pop();
              }
            )
          ]
        );
      }
    );
  }

  Future<void> _signOutFirebase() async
  {
    await FirebaseAuth.instance.signOut();
    if(mounted)
    {
      context.go('/login');
    }
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        )
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: Center(
        child: Column(
          children: [
            Card(
              child: Column(
                children: [
                  ListTile(
                    title: Text('Username:',
                    style: Theme.of(context).textTheme.titleSmall
                    ),
                    subtitle: Text('${FirebaseAuth.instance.currentUser!.displayName!}',
                    style: const TextStyle(fontFamily: 'PTSansNarrow',
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0)
                    ),
                  ),
                  ListTile(
                    title: Text('Email:',
                    style: Theme.of(context).textTheme.titleSmall
                    ),
                    subtitle: Text('${FirebaseAuth.instance.currentUser!.email!}',
                    style: const TextStyle(fontFamily: 'PTSansNarrow',
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0)
                    ),
                  ),
                  ElevatedButton(
                    onPressed: _signOutFirebase,
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    ),
                    child: const Text('Log out'),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            //add here
            Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ElevatedButton(
              onPressed: () => context.go('/sharedrecipes'),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
              child: const Text('View Shared Recipes   >')
            ),
          ]),
            Expanded(
              child: StreamBuilder<List<Map<String, dynamic>>>(
                stream: getFriendsStream(),
                builder: (context, snapshot)
                {
                  if(snapshot.connectionState == ConnectionState.waiting)
                  {
                    return const CircularProgressIndicator();
                  }
                  if(snapshot.hasError)
                  {
                    return Text('Error: ${snapshot.error}');
                  }
                  var friends = snapshot.data ?? [];
                  var sent = friends.where((friend) => friend['type'] == 'sent');
                  var recvd = friends.where((friend) => friend['type'] == 'received');
                  var accepted = friends.where((friend) => friend['status'] == 'accepted');

                  return friends.isEmpty
                  ? const Center(
                    child: Text(
                      'No Friends Yet'
                    ),
                  )
                  : ListView(
                    children:[
                      ListTile(
                        title: Text(
                          'Current Friends',
                          style: Theme.of(context).textTheme.titleSmall
                        )
                      ),
                      ...accepted.map((friend) => ListTile(
                        title: Text(friend['name'] as String,
                        style: const TextStyle(fontFamily: 'PTSansNarrow',
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0)),
                        trailing: IconButton(
                          onPressed: ()
                          {
                            _deleteFriend(friend['userId'] as String);
                          },
                          icon:const Icon(Icons.delete),
                        )
                      )), 
                      ListTile(
                        title: Text(
                          'Friend Requests',
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ),
                        ...recvd.map((friend) => ListTile(
                          title: Text(friend['name'] as String,
                          style: const TextStyle(fontFamily: 'PTSansNarrow',
                            fontWeight: FontWeight.bold,
                            fontSize: 20.0)
                          ),
                          trailing: PopupMenuButton(
                            itemBuilder: (context) =>
                            [
                              PopupMenuItem(
                                onTap: ()
                                {
                                  _approveFriend(friend['id'] as String);
                                },
                                child: const Text('Approve'),
                              ),
                              PopupMenuItem(
                                onTap: ()
                                {
                                  _deleteFriend(friend['id'] as String);
                                },
                                child: const Text('Delete')
                              )
                            ]
                          )
                          )),
                      ListTile(
                        title: Text(
                          'Sent Requests',
                          style: Theme.of(context).textTheme.titleSmall,
                          ),
                      ),
                      ...sent.map((friend) => ListTile(
                        title: Text(friend['name'] as String),
                        subtitle: const Text('Pending'),
                        trailing: IconButton(
                          onPressed: ()
                          {
                            _deleteFriend(friend['id'] as String);
                          },
                          icon:const Icon(Icons.delete),
                        )
                      ))
                    ]
                  );
                }
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                onPressed: _showAddFriendDialog,
                child: const Text('+ Friend')
              )
            )
          ],
        ),
      ),
    );
  }   
}
