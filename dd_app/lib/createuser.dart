
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:go_router/go_router.dart';
import 'main.dart';

class CreateUser extends StatefulWidget
{
  const CreateUser({super.key, required this.title});
  final String title;

  @override
  State<CreateUser> createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser>
{
  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _pwdController = TextEditingController();
  final _usernameController = TextEditingController();
  
  void _showErrorDialog(String msg)
  {
    showDialog(
      context: context,
      builder: (BuildContext context)
      {
        return AlertDialog(
          title: const Text('Error creating Account'),
          content: Text(msg),
          actions: <Widget> [
            TextButton(
              onPressed: ()
              {
                Navigator.of(context).pop();
              },
              child: const Text('OK')
            )
          ]
        );
      }
    );
  }

  Future<void> _createAccount() async
  {
    if(_formKey.currentState != null && _formKey.currentState!.validate())
    {
      final email = _emailController.text.trim();
      final pwd = _pwdController.text.trim();
      final username = _usernameController.text.trim();
      
      final usernameExists = await FirebaseFirestore.instance
        .collection('users')
        .where('username', isEqualTo: username)
        .get()
        .then((snapshot) => snapshot.docs.isNotEmpty);

        if(usernameExists)
        {
          _showErrorDialog('the username $username is already taken. Please choose another.');
          return;
        }

      try
      {

        UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: email, 
          password: pwd,
        );

        await userCredential.user!.updateDisplayName(username);
        await userCredential.user!.reload();

        await FirebaseFirestore.instance
          .collection('users')
          .doc(userCredential.user!.uid)
          .set({
            'username': username,
            'email': email,
          });

        print('Account created.');
        context.go('/');
      }
      catch (e)
      {
        if(e is FirebaseAuthException)
        {
          _showErrorDialog(e.message ?? "Unknown Error");
        }
        else
        {
          _showErrorDialog('Please Try Again.');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Form (
        key: _formKey,
        child: Center(
          child: Column(
            children: [
              Image.asset(
                'assets/logo_letter.png',
                height: 200,
                width: 200,
              ),
              TextFormField(
                controller: _usernameController,
                decoration: const InputDecoration(
                  labelText: 'Username',
                  icon: Icon(Icons.person),
                ),
                validator: (value)
                {
                  if(value == null || value.isEmpty)
                  {
                    return 'Please enter a username.';
                  }
                  return null;
                }
              ),
              TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(
                  labelText: 'Email',
                  icon: Icon(Icons.mail),
                ),
                validator: (value)
                {
                  if(value == null || value.isEmpty)
                  {
                    return 'Please enter your email address.';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _pwdController,
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                decoration: const InputDecoration(
                  labelText: 'Password',
                  icon: Icon(Icons.lock),
                ),
                validator: (value)
                {
                  if(value == null || value.isEmpty)
                  {
                    return 'Please enter a password.';
                  }
                  return null;
                },
              ),
              TextFormField(
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                decoration: const InputDecoration(
                  labelText: 'Confirm Password',
                  icon: Icon(Icons.lock),
                ),
                validator:(value)
                {
                  if(value == null || value.isEmpty)
                  {
                    return 'Please re-enter your password';
                  }
                  if(value != _pwdController.text)
                  {
                    return 'Passwords do not match. Retry.';
                  }
                  return null;
                },
              ),
              ElevatedButton(
                onPressed: _createAccount,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor),
                  foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
                  ),
                child: const Text('Create')
              ),
            ],
          ),
        ),
      ),
     ),
    );
  }  
}