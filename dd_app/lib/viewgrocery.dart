 import 'package:dd_app/main.dart';
 import 'package:flutter/material.dart';
 import 'package:cloud_firestore/cloud_firestore.dart';
 import 'package:firebase_auth/firebase_auth.dart';
 import 'package:go_router/go_router.dart';
 
 class ViewGroceryPage extends StatefulWidget
 {
  const ViewGroceryPage({super.key, required this.title});
  final String title;

  @override
  State<ViewGroceryPage> createState() => _ViewGroceryPageState();
 }

class _ViewGroceryPageState extends State<ViewGroceryPage>
{
  final TextEditingController _itemController = TextEditingController();

  Future <void> _addItem(String item, List<String> recipes) async
  {
    final currentUser = FirebaseAuth.instance.currentUser;
    if(currentUser == null)
    {
      return;
    }

    final snapshot = await FirebaseFirestore.instance
      .collection('users')
      .doc(currentUser.uid)
      .collection('groceryList')
      .doc(item).get();

    if(!snapshot.exists)
    {
        FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .collection('groceryList')
        .doc(item)
        .set({'item': item, 'recipes': recipes})
        .then((_) {
        _itemController.clear();
      });
    }
    else
    {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Item already exists.'),
          duration: Duration(seconds: 2)
          ),
      );
    }
  }

  @override
  Widget build(BuildContext context)
  {
    final currentUser = FirebaseAuth.instance.currentUser;
    if(currentUser == null)
    {
      return const Scaffold(
        body: Center(
          child: Text('User not logged in'),
        )
      );
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        leading: Image.asset(
          'assets/logo.png',
          height: 100,
          width: 100,
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
            onPressed:() => context.go('/'),
            icon: const Icon(Icons.home),
            ),
          IconButton(
            onPressed: () => context.go('/allrecipes'),
            icon: const Icon(Icons.checklist_rounded),
          ),
          IconButton(
            onPressed: () => context.go('/viewgrocery'),
            icon: const Icon(Icons.local_grocery_store),
          ),
          IconButton(
            onPressed: () => context.go('/profile'),
            icon: const Icon(Icons.person),
          ),
          ],
        )
      ),
      body: Column(
        children: [
          Expanded(
            child: StreamBuilder<QuerySnapshot> (
              stream: FirebaseFirestore.instance
              .collection('users')
              .doc(currentUser.uid)
              .collection('groceryList').snapshots(),
              builder: (context, snapshot)
              {
                if(snapshot.hasError)
                {
                  return Text('Error: ${snapshot.error}');
                }
                else if(!snapshot.hasData)
                {
                  return const Center(child: Text('No groceries yet'));
                }
                  final groceryItems = snapshot.data!.docs;
                  return ListView.builder(
                    itemCount: groceryItems.length,
                    itemBuilder: (context, index)
                    {
                      var groceryItem = groceryItems[index].data() as Map<String, dynamic>;
                      var itemName = groceryItem['item'] as String;
                      var recipes = List.from(groceryItem['recipes'] as List);

                      return ListTile(
                        leading: Checkbox(
                        value: false,
                        onChanged: (bool? newVal)
                        {
                          if(newVal!)
                          {
                            print('deleted');
                            FirebaseFirestore.instance
                            .collection('users')
                            .doc(currentUser.uid)
                            .collection('groceryList')
                            .doc(itemName)
                            .delete().catchError((error) => print('failed to delete document: $error'));
                          }
                        }),
                        title: Text(itemName,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        )),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children:[
                            const Text('Item For: ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          ...recipes.map<Widget>((recipe)
                          {
                            return GestureDetector(
                              onTap: ()
                              {
                                FirebaseFirestore.instance
                                  .collection('users')
                                  .doc(FirebaseAuth.instance.currentUser!.uid)
                                  .collection('sharedRecipes')
                                  .doc(recipe).get().then((snapshot)
                                {
                                  if(snapshot.exists)
                                  {
                                    context.go('/viewsharedrecipe/$recipe');
                                    print('shared recipe');
                                  }
                                  else if(recipe != 'None')
                                  {
                                    context.go('/viewrecipe/$recipe');
                                    print('original recipe');
                                  }
                              });
                              },
                              child: Text(recipe,
                              style: const TextStyle(
                                color: MyColors.AccentColor,
                                fontFamily: 'PTSansNarrow',
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                              )),
                            );
                          }),
                          ]
                        ),
                      );
                    }
                  );
              }
            )
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _itemController,
                    style: const TextStyle(fontFamily: 'PTSansNarrow'),
                    decoration: const InputDecoration(
                      hintText: 'Enter Item',
                      border: OutlineInputBorder(),
                      contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
                    ),
                  )
                ),
                TextButton(
                  onPressed: () {
                    _addItem(_itemController.text, ['None']);
                  },
                  style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(MyColors.AccentColor)
                  ),
                  child: const Text('+',
                    style: TextStyle(fontSize: 50)
                  )
                )
              ],
            )
          )
        ]
      ),
    );
  }
}