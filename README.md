﻿**DISH DYNAMO**

**Members:**

- Serina Garcia

**Project Description:**

- Dish Dynamo is a recipe application designed to encourage home cooking by creating a hub to store your homemade recipes. This application allows users to document their cooking experiences and save them for future uses or to share with others. Users can update ingredient lists with step-by-step instructions and even attach photos of their masterpieces. Furthermore, users can add items from a recipe to a grocery list. For each grocery item, Dish Dynamo will allow you to see an “Item for” feature to allow users to see what recipe the item is for (per the user’s selection).
- I will implement internet connectivity to enable users to access their collection on multiple devices. Using Cloud Firestore will ensure that users can switch between devices and allow users to sync their recipes. Also, this connectivity can support recipe sharing with friends and family, creating a community through culinary advancements. I will also implement a camera feature to capture and attach photos to their recipes. The camera allows for a personal touch from the user and a visual guide for the cooking process.

**Project Deliverables:**

|Expected Deliverables||
| - | :- |
|User Authentication|Implement a secure user authentication system to allow users to create accounts and log in securely|
|Recipe Creation and Editing|Allow users to create recipes, including adding ingredients, step-by-step instructions, and optional photos|
|Recipe Management|Enable users to view, edit, and delete saved recipes|
|Grocery List|Allow users to add ingredients from recipes to a grocery list with the ability to view and manage the list|
|Item Reference|Implement an “Item For” feature allowing users to see what recipe each item is associated with|
|Internet Connectivity|Establish internet connectivity to enable users to access their recipes collectively across devices|
|Cloud Firestore|Use Cloud Firestore for data storage to ensure recipes are synced across devices|
|Adding Friends|Allow users to add friends with the capability to share recipes|
|Recipe Sharing|Implement functionality for users to share recipes with friends|
|Camera Feature|Integrate a camera feature to allow users to capture and attach photos to their recipes.|



|Bonus Deliverables||
| - | :- |
|Search|A search feature to filter through recipes based on keywords or ingredients|
|Recipe Rating|Implement a 5-star rating system for users to provide feedback on recipes|
|Cooking Timer|Put in timers within recipes to assist users in managing cooking times|
|Favorites|Adding a favorites feature to filter through the recipes|

**Timeline:**

|Week|Milestone|
| - | - |
|1|User authentication, UI, recipe creation|
|2|Recipe editing and management|
|3|Grocery list and item reference|
|4|Internet connectivity and Cloud Firestore|
|5|Adding friends and recipe sharing|
|6|Camera feature, UI, and bonus deliverables (Search and Favorites first)|

